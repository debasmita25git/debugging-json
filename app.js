const https = require('https');

https.get('https://coderbyte.com/api/challenges/json/age-counting', (resp) => {

  let content = '';

 // push data character by character to "content" variable

 resp.on('data', (c) => content += c);

 // when finished reading all data, parse it for what we need now

 resp.on('end', () => {

   let jsonContent = JSON.parse(content);

   let jsonContentArray = jsonContent.data.split(',');

   let keyArray = [];

   for (let i = 0; i < jsonContentArray.length; i++) {

     let keySplit = jsonContentArray[i].trim().split('=');

     if(i >= 10 && i <= 15){ // select only index between 10 to 15
        //console.log('INDEX NO 10 to 15 -------------',i)
        if(keySplit[0] === 'key'){ // select only key IDs
            keyArray.push(keySplit[1]); // push the key values in between index 10 to 15
        }
    }

   }

   console.log(keyArray.toString()); // print the values with key IDs in between index 10 to 15



 });



});